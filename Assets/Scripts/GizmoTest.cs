﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmoTest : MonoBehaviour
{
    public GameObject start;

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(start.transform.position, transform.position);
    }
}
