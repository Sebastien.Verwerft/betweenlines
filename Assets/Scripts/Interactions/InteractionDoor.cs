﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InteractionDoor : MonoBehaviour
{
    public Animator animatorDoor;

    public LoadingScreen loadingScreen;

    private bool isOpened = false;

    public GameObject player;

    public void StartAnimation()
    {
        animatorDoor.SetBool("openDoor", true);
        if(!isOpened)
        {
            GetComponent<AudioSource>().Play();
        }
        isOpened = true;
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && isOpened)
        {
            player.transform.GetChild(0).GetComponent<Animator>().SetTrigger("Trigger");
            player.GetComponent<PlayerMovement>().beginAnimEnded = false;
            loadingScreen.StartCoroutine(loadingScreen.LoadLevelAnim(SceneManager.GetActiveScene().buildIndex + 1));
        }
    }

}
