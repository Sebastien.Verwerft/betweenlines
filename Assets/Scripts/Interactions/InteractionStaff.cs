﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class InteractionStaff : MonoBehaviour
{
    private GameObject player;

    private PlayerInteractions playerInteractions;

    public AudioClip audioClip;

    private Camera cameraMain;
    private TextMeshProUGUI textOnRight;

    public TextMeshProUGUI textInteraction;
    public Image imageInteraction;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Eliott").gameObject;
        playerInteractions = player.GetComponent<PlayerInteractions>();

        cameraMain = GameObject.Find("Camera").GetComponent<Camera>();

        if (transform.Find("TextInteraction") != null)
        {
            textOnRight = transform.GetChild(2).transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            playerInteractions.canInteract = true;
            playerInteractions.tagInteraction = this.tag;
            playerInteractions.SetInteractiveObject(gameObject, textOnRight);
            SetInteractTextPosition(transform.GetChild(1).position);
            StartCoroutine(ShowHideTextInteraction(0.5f, true));
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            playerInteractions.canInteract = false;
            transform.GetChild(1).gameObject.SetActive(false);
            StartCoroutine(ShowHideTextInteraction(0.5f, false));
        }
    }

    private void SetInteractTextPosition(Vector3 positionSprite)
    {
        Vector3 screenPos = cameraMain.WorldToScreenPoint(positionSprite);
        textInteraction.gameObject.transform.position = new Vector3(screenPos.x + 220f, screenPos.y - 50f, screenPos.z);
        imageInteraction.gameObject.transform.position = new Vector3(screenPos.x, screenPos.y - 50f, screenPos.z);
    }

    IEnumerator ShowHideTextInteraction(float timeLerp, bool afficher)
    {
        float timeElapsed = 0.0f;
        float value = 0.0f;

        while (timeElapsed < timeLerp)
        {
            float t = timeElapsed / timeLerp;

            if (afficher)
            {
                value = Mathf.Lerp(0, 1, t);
            }
            else
            {
                value = Mathf.Lerp(1, 0, t);
            }

            textInteraction.color = new Vector4(255, 255, 255, value);
            imageInteraction.color = new Vector4(255, 255, 255, value);

            if (value < 0.1f)
            {
                textInteraction.color = new Vector4(255, 255, 255, 0);
                imageInteraction.color = new Vector4(255, 255, 255, 0);
            }

            timeElapsed += Time.deltaTime;
            yield return null;
        }
    }
}
