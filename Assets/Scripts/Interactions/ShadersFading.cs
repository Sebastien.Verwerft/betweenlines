﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadersFading : MonoBehaviour
{

    public float timeLerpHG;
    public float timeLerpEliott;

    private GameObject[] objShaderHG;
    private GameObject[] objShaderHG2;
    private GameObject[] objShaderEliott;

    private bool setVisible = true;

    private Maths maths;

    private void Start()
    {
        maths = GetComponent<Maths>();
        objShaderHG = GameObject.FindGameObjectsWithTag("ShaderHG");
        objShaderHG2 = GameObject.FindGameObjectsWithTag("ShaderHG2");
        objShaderEliott = GameObject.FindGameObjectsWithTag("ShaderEliott");
    }

    public IEnumerator HGshader()
    {
        float timeElapsed = 0.0f;
        float value = 0.0f;
        int temp = 0;

        while (timeElapsed < timeLerpHG)
        {
            float t = timeElapsed / timeLerpHG;
            value = Mathf.Lerp(0, 1, maths.SmoothStop2(t));

            foreach (GameObject obj in objShaderHG)
            {
                temp = obj.transform.GetComponent<Renderer>().materials.Length;

                for(int i = 0; i < temp; i++)
                {
                    if(setVisible)
                    {
                        obj.transform.GetComponent<Renderer>().materials[i].SetFloat("_Opacity", value);
                    }
                    else
                    {
                        obj.transform.GetComponent<Renderer>().materials[i].SetFloat("_Opacity", 1 - value);
                    }
                }

            }
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        setVisible = !setVisible;
    }

    public IEnumerator HGshader2(bool isZooming)
    {
        float timeElapsed = 0.0f;
        int temp = 0;

        while (timeElapsed < timeLerpHG)
        {
            float t = timeElapsed / timeLerpHG;
            float value = Mathf.Lerp(0, 1, maths.SmoothStop2(t));

            foreach (GameObject obj in objShaderHG2)
            {
                temp = obj.transform.GetComponent<Renderer>().materials.Length;

                for (int i = 0; i < temp; i++)
                {
                    if (!isZooming)
                    {
                        obj.transform.GetComponent<Renderer>().materials[i].SetFloat("_Opacity", value);
                    }
                    else
                    {
                        obj.transform.GetComponent<Renderer>().materials[i].SetFloat("_Opacity", 1 - value);
                    }
                }

            }
            timeElapsed += Time.deltaTime;
            yield return null;
        }
    }

    public IEnumerator EliottShader(bool isZooming)
    {
        float timeElapsed = 0.0f;

        while (timeElapsed < timeLerpEliott)
        {
            float t = timeElapsed / timeLerpEliott;
            float value = Mathf.Lerp(0, 1, maths.SmoothStop2(t));

            foreach (GameObject obj in objShaderEliott)
            {
                if (isZooming)
                {
                    obj.transform.GetComponent<Renderer>().materials[0].SetFloat("_Opacity", 1 - value);
                }
                else
                {
                    obj.transform.GetComponent<Renderer>().materials[0].SetFloat("_Opacity", value);
                }
            }

            timeElapsed += Time.deltaTime;
            yield return null;
        }
    }
}
