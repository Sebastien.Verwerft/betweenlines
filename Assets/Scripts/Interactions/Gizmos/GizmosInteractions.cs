﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmosInteractions : MonoBehaviour
{
    private GameObject[] interactions;

    private void OnDrawGizmos()
    {
        interactions = new GameObject[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        {
            interactions[i] = transform.GetChild(i).gameObject;

            if (interactions[i].activeSelf)
            {
                Gizmos.color = Color.green;
                Gizmos.matrix = interactions[i].transform.localToWorldMatrix;
                Gizmos.DrawWireCube(Vector3.zero + interactions[i].GetComponent<BoxCollider>().center, interactions[i].GetComponent<BoxCollider>().size);
            }
        }
    }
}
