﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TestMovement : MonoBehaviour
{
    public GameObject start;
    public GameObject end;

    public GameObject start2;
    public GameObject variant;
    public GameObject end2;

    public float lerpDuration;

    public Maths math;

    public GameObject sphere;
    public GameObject sphere1;

    public int numberOfPoints;

    public bool salut = false;

    // Start is called before the first frame update
    void Start()
    {
        sphere.transform.position = start.transform.position;
        sphere1.transform.position = start2.transform.position;
    }

    public void TestInput(InputAction.CallbackContext ctx)
    {
        if(ctx.started)
        {
            salut = !salut;
            StartCoroutine(Moving());
            StartCoroutine(MovingBezier());
        }
    }

    private IEnumerator Moving()
    {
        float timeElapsed = 0f;

        while (timeElapsed < lerpDuration)
        {
            float t = timeElapsed / lerpDuration;

            t = math.CameraHermiteMix(t);

            Debug.Log(t);

            sphere.transform.position = Vector3.Lerp(start.transform.position, end.transform.position, t);

            timeElapsed += Time.deltaTime;
            yield return null;
        }
    }

    private IEnumerator MovingBezier()
    {
        float timeElapsed = 0.0f;

        // set points of quadratic Bezier curve
        Vector3 p0 = start2.transform.position;
        Vector3 p1 = end2.transform.GetChild(0).transform.position;
        Vector3 p2 = end2.transform.position;

        while (timeElapsed < lerpDuration)
        {
            float t = timeElapsed / lerpDuration;

            float value = Mathf.Lerp(0, 1, math.CameraHermiteMix(t));

            if(salut)
            {
                sphere1.transform.position = (1.0f - value) * (1.0f - value) * p0 + 2.0f * (1.0f - value) * value * p1 + value * value * p2;
            }
            else
            {
                sphere1.transform.position = (1.0f - value) * (1.0f - value) * p2 + 2.0f * (1.0f - value) * value * p1 + value * value * p0;
            }


            timeElapsed += Time.deltaTime;
            yield return null;
        }
    }
}
