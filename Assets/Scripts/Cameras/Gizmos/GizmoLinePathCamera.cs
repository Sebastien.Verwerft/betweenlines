﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class GizmoLinePathCamera : MonoBehaviour
{
    public GameObject start;
    private GameObject variant;

    public Color color = Color.white;
    public float width = 0.02f;

    public int numberOfPoints = 20;

    LineRenderer lineRenderer;

    private void OnDrawGizmos()
    {
        if(lineRenderer == null)
        {
            lineRenderer = GetComponent<LineRenderer>();
            lineRenderer.useWorldSpace = true;
            lineRenderer.material = new Material(Shader.Find("Legacy Shaders/Particles/Additive"));
            
            variant = transform.GetChild(0).gameObject;
        }

        // update line renderer
        lineRenderer.startColor = color;
        lineRenderer.endColor = color;
        lineRenderer.startWidth = width;
        lineRenderer.endWidth = width;
        
        // set points of quadratic Bezier curve
        Vector3 p0 = start.transform.position;
        Vector3 p1 = variant.transform.position;
        Vector3 p2 = transform.position;
        Vector3 position;

        float t;


        if (numberOfPoints > 0)
        {
            lineRenderer.positionCount = numberOfPoints;
        }

        for (int i = 0; i < numberOfPoints; i++)
        {
            t = i / (numberOfPoints - 1.0f);

            position = (1.0f - t) * (1.0f - t) * p0
            + 2.0f * (1.0f - t) * t * p1 + t * t * p2;
            lineRenderer.SetPosition(i, position);
        }
    }
}
