﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmoMiddleOfPath : MonoBehaviour
{
    public GameObject start;
    public bool setVariantMiddlePosition = false;

    private GameObject end;

    private Vector3 middlePosition;

    private void OnDrawGizmos()
    {
        end = transform.parent.gameObject;

        middlePosition = new Vector3(start.transform.position.x + (end.transform.position.x - start.transform.position.x) / 2, start.transform.position.y + (end.transform.position.y - start.transform.position.y) / 2, start.transform.position.z + (end.transform.position.z - start.transform.position.z) / 2);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(middlePosition, 0.05f);
        
        if(setVariantMiddlePosition)
        {
            transform.position = middlePosition;
        }
    }
}
