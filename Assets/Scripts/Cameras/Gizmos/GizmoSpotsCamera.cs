﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmoSpotsCamera : MonoBehaviour
{
    private GameObject[] cameraSpots;

    private void OnDrawGizmos()
    {
        cameraSpots = new GameObject[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        {
            cameraSpots[i] = transform.GetChild(i).gameObject;

            if(cameraSpots[i].activeSelf)
            { 
                if(i == 0)
                {
                    Gizmos.color = Color.yellow;
                    Gizmos.DrawWireSphere(cameraSpots[0].transform.position, 0.2f);
                }
                else
                {
                    Gizmos.color = Color.blue;
                    Gizmos.DrawWireSphere(cameraSpots[i].transform.position, 0.2f);

                }
            }
        }
    }
}
