﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;


public class PlayerCamera : MonoBehaviour
{   
    //////////////////
    // Public Values
    //////////////////
    public float sensitivity = 100f;
    public float height = 0.5f;
    public float distance = 8f;
    public float clampeAngleUp = 50.0f;
    public float clampeAngleDown = 50.0f;

    //////////////////
    // Private Values
    //////////////////
    private GameObject pivot;
    private Transform pivotXaxis, pivotYaxis, CameraT;

    private MenuManager menuManager;

    private Vector2 lookInputValues;
    private Vector3 offset;
    private Vector3 velocity = Vector3.zero;

    private float rotX;
    private float rotY;

    // Start is called before the first frame update
    void Start()
    {
        // Hide Cursor
        Cursor.visible = false;

        // Get all references for Camera
        pivot = GameObject.Find("CameraPlayer");
        pivotXaxis = pivot.transform.Find("pivotCameraXaxis");
        pivotYaxis = pivot.transform.Find("pivotCameraYaxis");
        CameraT = pivot.transform.Find("Camera");

        // Set the pivot GO at the same position of the player + set the offset
        pivot.transform.position = transform.position;
        offset = new Vector3(pivot.transform.position.x, pivot.transform.position.y,  -distance);
        menuManager = GameObject.Find("MenuManager").GetComponent<MenuManager>();
    }

    private void FixedUpdate()
    {

        // Get input values and add it with speed
        rotX = lookInputValues.x * sensitivity * Time.deltaTime;
        rotY = lookInputValues.y * sensitivity * Time.deltaTime;

        // Set the pivot GO at the same position of the player + rotate pivots
        pivot.transform.position = new Vector3(transform.position.x, transform.position.y + height, transform.position.z);
        pivotYaxis.Rotate(0, rotX, 0f);
        pivotXaxis.Rotate(-rotY, 0f, 0f);

        // Clamp the vertical Up rotation
        if (pivotXaxis.rotation.eulerAngles.x > clampeAngleUp && pivotXaxis.rotation.eulerAngles.x < 180f)
        {
            pivotXaxis.rotation = Quaternion.Euler(clampeAngleUp, 0, 0);
        }

        //Clamp the vertical down rotation
        if (pivotXaxis.rotation.eulerAngles.x > 180f && pivotXaxis.rotation.eulerAngles.x < 360f - clampeAngleDown)
        {
            pivotXaxis.rotation = Quaternion.Euler(360f - clampeAngleDown, 0, 0);
        }

        // Store the angle values
        float desiredXAngle = pivotXaxis.eulerAngles.x;
        float desiredYAngle = pivotYaxis.eulerAngles.y;

        // Create new rotation and move the camera from it
        Quaternion rotation = Quaternion.Euler(desiredXAngle, desiredYAngle, 0);
        CameraT.transform.position = Vector3.SmoothDamp(CameraT.transform.position, pivot.transform.position + (rotation * offset), ref velocity, 0.1f);

        // Clamp the position of the camera to the pivot GO position
        if(CameraT.transform.position.y < pivot.transform.position.y)
        {
            CameraT.transform.position = new Vector3(CameraT.transform.position.x, pivot.transform.position.y, CameraT.transform.position.z);
        }

        // Camera look the pivot GO
        CameraT.transform.LookAt(pivot.transform);
    }


    // Store the value from the input system
    public void GetLookValues(InputAction.CallbackContext context)
    {
        lookInputValues = context.ReadValue<Vector2>();
    }
}
