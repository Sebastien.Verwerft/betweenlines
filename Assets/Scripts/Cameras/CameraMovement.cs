﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    /////////////////////
    /// Private values
    /////////////////////

    private Maths maths;
    private PlayerInteractions playerInteractions;
    private PlayerMovement playerMovement;

    /////////////////////
    /// Public values
    /////////////////////

    public GameObject Oven;
    public GameObject OvenTwo;
    public GameObject OvenTwoLookAt;

    public float timeLerpOven;
    public float OvenTwoSensitivity;
    public float OvenTwoSensitivityReverse;

    public bool isZooming = false;
    public bool isEndCameraSpot = false;

    public int numberOfPoints;

    public GameObject cameraSpotStart;
    public GameObject LookAtGlobalPointView;

    public GameObject player;

    private ShadersFading shadersFading;

    private Quaternion startRotation;

    public InteractionDoor interactionDoor;

    public AudioSource HGaudioSource;
    public AudioClip HGclip;

    private void Start()
    {
        maths = this.GetComponent<Maths>();
        shadersFading = GameObject.Find("ShaderMat").GetComponent<ShadersFading>();
        playerInteractions = GameObject.Find("Eliott").GetComponent<PlayerInteractions>();
        playerMovement = GameObject.Find("Eliott").GetComponent<PlayerMovement>();
        cameraSpotStart = GameObject.Find("CameraSpotViews").transform.GetChild(0).gameObject;
        LookAtGlobalPointView = GameObject.Find("LookAtGlobalPointView");
        transform.position = cameraSpotStart.transform.position;
        transform.rotation = cameraSpotStart.transform.rotation;

        startRotation = transform.rotation;
    }

    public IEnumerator LerpCameraMovement(GameObject start, GameObject end, float timeLerp, PlayerInteractions.MathMovementDelegate mathfunction)
    {
        float timeElapsed = 0.0f;
        playerInteractions.canInteract = false;

        int temp = player.transform.childCount;

        for(int i = 0; i < temp; i++)
        {
            if(player.transform.GetChild(i).GetComponent<Renderer>() != null)
            {
                player.transform.GetChild(i).GetComponent<Renderer>().enabled = false;
            }
        }

        // set points of quadratic Bezier curve
        Vector3 p0 = start.transform.position;
        Vector3 p1 = end.transform.GetChild(0).transform.position;
        Vector3 p2 = end.transform.position;

        if(!isZooming && isEndCameraSpot)
        {
            p0 = cameraSpotStart.transform.position;
            p1 = cameraSpotStart.transform.GetChild(0).transform.position;
            p2 = OvenTwo.transform.position;
            isEndCameraSpot = false;
            shadersFading.StartCoroutine(shadersFading.HGshader2(!isZooming));
        }

        while (timeElapsed < timeLerp)
        {
            float t = timeElapsed / timeLerp;
            float value = Mathf.Lerp(0, 1, mathfunction(t));

            if (isZooming)
            {
                transform.position = maths.BezierCurve(value, p0, p1, p2);
            }
            else
            {
                transform.position = maths.BezierCurveReverse(value, p0, p1, p2);
            }

            timeElapsed += Time.deltaTime;
            yield return null;
        }

        if (!isZooming)
        {
            playerMovement.currentSpeed = playerMovement.moveSpeed;

            if(playerInteractions.openDoor == true)
            {
                interactionDoor.StartAnimation();
            }

            playerInteractions.StartCoroutine(playerInteractions.ShowHideTextInteraction(0.5f, false));
        }

        if (end == Oven && isZooming)
        {
            StartCoroutine(Wait(4));
            HGaudioSource.Play();
        }
        else
        {
            playerInteractions.canInteract = true;
        }
    }

    public IEnumerator LerpCameraRotation(float timeLerp, GameObject lookAtTarget, GameObject lookAtDefault, PlayerInteractions.MathRotationDelegate mathfunction, PlayerInteractions.MathRotationBackDelegate mathfunctionback, float sensitivity, float sensitivityReverse)
    {
        float timeElapsed = 0.0f;

        while (timeElapsed < timeLerp)
        {
            float v = timeElapsed / timeLerp;
            
            
            if(isZooming)
            {
                v = mathfunction(v);
                LookAtTarget(lookAtTarget.transform.position, v * sensitivity);
            }
            else if (!isZooming)
            {
                v = mathfunctionback(v);
                transform.rotation = Quaternion.Slerp(transform.rotation, startRotation, v * sensitivityReverse * Time.deltaTime);
            }

            timeElapsed += Time.deltaTime;
            yield return null;
        }
    }

    private void LookAtTarget(Vector3 look, float speedFocus)
    {
        Vector3 direction = look - transform.position;
        Quaternion desiredRotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, desiredRotation, speedFocus * Time.deltaTime);
    }

    IEnumerator Wait(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        shadersFading.StartCoroutine(shadersFading.HGshader());
        shadersFading.StartCoroutine(shadersFading.HGshader2(!isZooming));

        StartCoroutine(LerpCameraMovement(Oven, OvenTwo, timeLerpOven, maths.CameraHermiteMix));
        StartCoroutine(LerpCameraRotation(timeLerpOven, OvenTwoLookAt, LookAtGlobalPointView, maths.SmoothStop3, maths.Linear, OvenTwoSensitivity, OvenTwoSensitivityReverse));
        isEndCameraSpot = true;

        yield return new WaitForSeconds(2f);
        HGaudioSource.clip = HGclip;
        HGaudioSource.Play();
    }

}
