﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampFireStep : MonoBehaviour
{
    //////////////////
    // Public Values
    //////////////////
    public GameObject cube;

    public float duration;

    //////////////////
    // Private Values
    //////////////////
    private float alphaColor = 0.0f;
    private float lerp = 0.0f;
    
    private bool playerEnter = false;


    // Update is called once per frame
    void Update()
    {
        // Enter the area and display shaders
        if (playerEnter && alphaColor < 1f)
        {
            alphaColor = Mathf.Lerp(0.0f, 1.0f, lerp);
            lerp += duration * Time.deltaTime;

            cube.GetComponent<Renderer>().material.SetFloat("Ghost_Transparency", alphaColor);
        }

        // Get out of area and hide shaders
        else if (!playerEnter && alphaColor > 0f)
        {
            alphaColor = Mathf.Lerp(1.0f, 0.0f, lerp);
            lerp += duration * Time.deltaTime;

            cube.GetComponent<Renderer>().material.SetFloat("Ghost_Transparency", alphaColor);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            lerp = 0.0f;
            playerEnter = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            lerp = 0.0f;
            playerEnter = false;
        }
    }
   
}
