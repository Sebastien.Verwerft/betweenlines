﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SubTitles : MonoBehaviour
{
    private TextMeshProUGUI subTitle;

    private SoundsInScene audioSource;

    private void Awake()
    {
        subTitle = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        audioSource = GameObject.Find("Audio Source").GetComponent<SoundsInScene>();
    }

    public IEnumerator Sequence(float timeBegin, string text, AudioClip clip, float timeEnd)
    {
        if(MenuManager.subtitles)
        {
            subTitle.text = text;
            yield return new WaitForSeconds(timeBegin);
            StartCoroutine(ShowHideTextInteraction(1f, true));
            audioSource.PlaySound(clip);
            yield return new WaitForSeconds(timeEnd);
            StartCoroutine(ShowHideTextInteraction(1f, false));
        }
        else
        {
            yield return new WaitForSeconds(2f);
            audioSource.PlaySound(clip);
        }
    }

    public IEnumerator SequenceWithoutSound(float timeBegin, string text, float timeEnd)
    {
        if (MenuManager.subtitles)
        {
            subTitle.text = text;
            yield return new WaitForSeconds(timeBegin);
            StartCoroutine(ShowHideTextInteraction(1f, true));
            yield return new WaitForSeconds(timeEnd);
            StartCoroutine(ShowHideTextInteraction(1f, false));
        }
        else
        {
            yield return new WaitForSeconds(2f);
        }
    }

    IEnumerator ShowHideTextInteraction(float timeLerp, bool afficher)
    {
        float timeElapsed = 0.0f;
        float value = 0.0f;

        while (timeElapsed < timeLerp)
        {
            float t = timeElapsed / timeLerp;

            if (afficher)
            {
                value = Mathf.Lerp(0, 1, t);
            }
            else
            {
                value = Mathf.Lerp(1, 0, t);
            }

            subTitle.color = new Vector4(255, 255, 255, value);

            if (value < 0.1f)
            {
                subTitle.color = new Vector4(255, 255, 255, 0);
            }

            timeElapsed += Time.deltaTime;
            yield return null;
        }
    }

}
