﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour
{
    private Animator transition;

    // Start is called before the first frame update
    void Start()
    {
        transition = GameObject.Find("Overlay").GetComponent<Animator>();
        
        if(this.gameObject.name == "Loading")
        {
            StartCoroutine(Wait());
        }
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(2);
        StartCoroutine(LoadLevelAnim(SceneManager.GetActiveScene().buildIndex + 1));
    }

    public IEnumerator LoadLevelAnim(int levelIndex)
    {
        transition.SetTrigger("FadeOut");

        yield return new WaitForSeconds(2f);

        //if(GameObject.FindGameObjectWithTag("Music") != null)
        //{
        //    GameObject.FindGameObjectWithTag("Music").GetComponent<MusicBetweenScenes>().PauseMusic();
        //}

        SceneManager.LoadScene(levelIndex);
    }
}
