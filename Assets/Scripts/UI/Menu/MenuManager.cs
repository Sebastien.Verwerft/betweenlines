﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.InputSystem.Users;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [Header("Main Menu")]
    public GameObject mainPlayButton;

    [Header("Main Menu options")]
    public GameObject backButton;

    [Header("PauseMenu")] 
    public GameObject pauseMenu;
    private Canvas canvasPause;
    public GameObject pauseBackButton;
    public bool isPaused;

    [Header("InputActions")]
    public PlayerInput inputPlayer;

    public string currentScheme;

    private GameObject defaultButton;

    public GameObject textVolume;

    public static bool subtitles = true;
    private static bool fullscreen = true;
    private static float volumeValue;

    private Maths maths;

    private GameObject nextMenu;
    private GameObject currentMenu;

    private LoadingScreen loadingScreen;
    private PlayerInteractions playerInteractions;

    public Image imageInteraction;
    public Sprite spriteGamePad;
    public Sprite spriteKeyboard;

    public GameObject subTitleText;
    public GameObject fullScreenText;

    public Slider slider;

    public AudioSource[] allAudioSources;

    private void Start()
    {
        loadingScreen = GetComponent<LoadingScreen>();

        if (SceneManager.GetActiveScene().name == "MainMenu")
        {
            Cursor.visible = true;

            inputPlayer = GetComponent<PlayerInput>();
            InputUser.onChange += ChangeControlScheme;
            currentScheme = inputPlayer.currentControlScheme;
            defaultButton = mainPlayButton;
            SetVolumeText(100);

            currentMenu = GameObject.Find("MainMenu");
            maths = GetComponent<Maths>();

            EventSystem.current.SetSelectedGameObject(defaultButton);

        }
        else 
        {
            playerInteractions = GameObject.Find("Eliott").GetComponent<PlayerInteractions>();
            maths = GetComponent<Maths>();

            Cursor.visible = false;

            imageInteraction.sprite = spriteKeyboard;

            inputPlayer = GameObject.Find("Eliott").GetComponent<PlayerInput>();
            InputUser.onChange += ChangeControlScheme;
            currentScheme = inputPlayer.currentControlScheme;

            pauseMenu = GameObject.Find("PauseMenu");
            currentMenu = pauseMenu;
            canvasPause = pauseMenu.GetComponent<Canvas>();
            pauseBackButton = pauseMenu.transform.Find("ReturnButton").gameObject;

            InitSettingsValues(subTitleText, fullScreenText, subtitles, fullscreen);
            
            SetVolumeText(volumeValue);
            slider.value = volumeValue;
            slider.GetComponentInParent<AudioSource>().enabled = true;

            allAudioSources = FindObjectsOfType<AudioSource>();

            defaultButton = pauseBackButton;
            pauseMenu.SetActive(false);
        }
    }

    void ChangeControlScheme(InputUser user, InputUserChange change, InputDevice device)
    {
        if (change == InputUserChange.ControlSchemeChanged)
        {
            currentScheme = user.controlScheme.Value.name;
            SelectDefaultButton(user.controlScheme.Value.name);
            UpdateButtonImage(user.controlScheme.Value.name);
        }
    }

    private void SelectDefaultButton(string schemeName)
    {
        if(schemeName.Equals("GamePad"))
        {
            EventSystem.current.SetSelectedGameObject(defaultButton);
        }
        else
        {
            EventSystem.current.SetSelectedGameObject(null);
        }
    }

    public void SetButtonAsDefault(GameObject go)
    {
        defaultButton = go;
    }

    public void SetNextButton(GameObject go)
    {
        defaultButton = go;

        if (currentScheme.Equals("GamePad"))
        {
            EventSystem.current.SetSelectedGameObject(defaultButton);
        }
    }

    void UpdateButtonImage(string schemeName)
    {
        if (schemeName.Equals("GamePad") && imageInteraction != null)
        {
            imageInteraction.sprite = spriteGamePad;
        }
        else
        {
            if(imageInteraction != null)
            {
                imageInteraction.sprite = spriteKeyboard;
            }
        }
    }

    public void QuitPauseMenu()
    {
        playerInteractions.isPaused = false;
        playerInteractions.SetGamePause();
    }

    public void PlayGame()
    {
        StartCoroutine(loadingScreen.LoadLevelAnim(SceneManager.GetActiveScene().buildIndex + 1));
        Cursor.visible = false;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void BackToMainMenu()
    {
        Time.timeScale = 1;
        StartCoroutine(loadingScreen.LoadLevelAnim(1));
        canvasPause.gameObject.SetActive(false);
    }

    public void SetVolumeText(float value)
    {
        textVolume.GetComponent<TextMeshProUGUI>().text = value.ToString();
        volumeValue = value;
    }

    private void InitSettingsValues(GameObject obj, GameObject obj2, bool sub, bool screen)
    {
        if (subtitles)
        {
            obj.GetComponent<TextMeshProUGUI>().text = "ON";
        }
        else
        {
            obj.GetComponent<TextMeshProUGUI>().text = "OFF";
        }

        if (fullscreen)
        {
            obj2.GetComponent<TextMeshProUGUI>().text = "ON";
        }
        else
        {
            obj2.GetComponent<TextMeshProUGUI>().text = "OFF";
        }
    }

    public void SubTitlesSetting(GameObject obj)
    {
        subtitles = !subtitles;
        
        if(subtitles)
        {
            obj.GetComponent<TextMeshProUGUI>().text = "ON";
        }
        else
        {
            obj.GetComponent<TextMeshProUGUI>().text = "OFF";
        }
    }

    public void FullscreenSetting(GameObject obj)
    {
        fullscreen = !fullscreen;
        
        if (fullscreen)
        {
            obj.GetComponent<TextMeshProUGUI>().text = "ON";
        }
        else
        {
            obj.GetComponent<TextMeshProUGUI>().text = "OFF";
        }
    }

    public void ApplySettings()
    {
        if (fullscreen)
        {
            Screen.fullScreen = true;
        }
        else
        {
            Screen.fullScreen = false;
        }

        SetVolume();
    }

    private void SetVolume()
    {
       for(int i = 0; i < allAudioSources.Length; i++)
       {
            allAudioSources[i].volume = volumeValue / 100;
       }
    }


    public void GetNextMenu(GameObject obj)
    {
        nextMenu = obj;
    }

    public void FadeInUI(GameObject obj)
    {
        List<GameObject> temp = obj.transform.FindObjectsWithTag("UIFade");
        StartCoroutine(FadeUI(temp, 1f, true));
    }

    private void FadeOutUI(GameObject obj)
    {
        List<GameObject> temp = obj.transform.FindObjectsWithTag("UIFade");
        StartCoroutine(FadeUI(temp, 1f, false));
    }

    IEnumerator FadeUI(List<GameObject> obj, float totalTime, bool isFadeIn)
    {
        float timeElapsed = 0.0f;
        float value;

        while (timeElapsed < totalTime)
        {
            float t = timeElapsed / totalTime;

            if(isFadeIn)
            {
                value = Mathf.Lerp(1, 0, maths.Linear(t));
            }
            else
            {
                value = Mathf.Lerp(0, 1, maths.Linear(t));
            }

            for(int i = 0; i < obj.Count; i++)
            {
                Image tempImage = obj[i].transform.GetComponent<Image>();

                if(tempImage == null)
                {
                    TextMeshProUGUI textProUI = obj[i].transform.GetComponent<TextMeshProUGUI>();
                    textProUI.color = new Vector4(textProUI.color.r, textProUI.color.g, textProUI.color.b, value);
                }
                else
                {
                    tempImage.color = new Vector4(tempImage.color.r, tempImage.color.g, tempImage.color.b, value);
                }

            }

            timeElapsed += Time.deltaTime;
            yield return null;
        }

        if(isFadeIn)
        {
            currentMenu.SetActive(false);
            nextMenu.SetActive(true);
            FadeOutUI(nextMenu);
            currentMenu = nextMenu;
        }
        else if(currentScheme == "GamePad")
        {
            EventSystem.current.SetSelectedGameObject(defaultButton);
        }

    }

    public void PlaySoundInMenu(AudioClip clip)
    {
        GameObject.Find("Audio Source").GetComponent<AudioSource>().clip = clip;
        GameObject.Find("Audio Source").GetComponent<AudioSource>().Play();
    }

    public void PlaySoundInPauseMenu(AudioSource source)
    {
        source.Play();
    }

    public void PauseSettings(bool isReverse)
    {
        if(isReverse)
        {
            nextMenu.transform.GetChild(0).GetComponent<Animator>().SetTrigger("TriggerReverse");
        }
        else
        {
            nextMenu.transform.GetChild(0).GetComponent<Animator>().SetTrigger("Trigger");
        }
    }

}
