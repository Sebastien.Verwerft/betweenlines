﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroductionScene : MonoBehaviour
{

    private bool isFirstTime = true;

    public AudioSource music;
    public AudioSource woolBall;
    public SoundsInScene soundsInScene;
    public SubTitles subTitles;
    public AudioClip clipBedroom;
    public AudioSource clipSmokeEnter;

    public Animator EliottFalling;
    public PlayerMovement playerMovement;

    public Animator smoke;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<AudioSource>().Play();
        subTitles.StartCoroutine(subTitles.SequenceWithoutSound(0.2f, "What is all this ruckus about ? Oh ! A new reader… \nHello ! It seems that you may have lost your way…\nNot to worry, I’ll help you get out of this place. \nThere must be something we can do !", 15.5f));
    }

    // Update is called once per frame
    void Update()
    {
        if(!GetComponent<AudioSource>().isPlaying && isFirstTime)
        {
            GetComponent<Animator>().SetBool("FadeOut", true);
            isFirstTime = false;
        }
    }

    private void StartAnimElliot()
    {
        EliottFalling.SetTrigger("Trigger");
        smoke.SetTrigger("Trigger");
        playerMovement.StartCoroutine(playerMovement.Wait(8f));
        subTitles.gameObject.transform.GetChild(0).position = new Vector2(subTitles.gameObject.transform.GetChild(0).position.x, subTitles.gameObject.transform.GetChild(0).position.y - 90f);
        woolBall.enabled = true;
        clipSmokeEnter.Play();
    }

    private void PlayEnterNarratorBedroom()
    {
        music.Play();
        subTitles.StartCoroutine(subTitles.Sequence(2f, "What a mess… Watch where you're stepping ! \nWe don't want you to hurt yourself !", clipBedroom, 9f));
        gameObject.SetActive(false);
    }

}
