﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blur : MonoBehaviour
{
    public Camera blurCamera;
    public Material blurMaterial;

    // Start is called before the first frame update
    void Start()
    {
        blurCamera.targetTexture = new RenderTexture(1920, 1080, 24, RenderTextureFormat.ARGB32, 1);
        blurMaterial.SetTexture("_RenTexture", blurCamera.targetTexture);
    }

}
