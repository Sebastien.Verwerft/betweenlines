﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class Cinematique : MonoBehaviour
{
    VideoPlayer video;

    public GameObject skipButton;

    private bool isFirstAppuie = true;

    void Awake()
    {
        video = GetComponent<VideoPlayer>();
        video.Play();
        video.loopPointReached += CheckOver;
    }

    void CheckOver(UnityEngine.Video.VideoPlayer vp)
    {
        SceneManager.LoadScene(1);
    }


    public void SkipCinematique(InputAction.CallbackContext ctx)
    {
        if(ctx.started)
        {
            if(isFirstAppuie)
            {
                skipButton.SetActive(true);
                isFirstAppuie = false;
            }
            else
            {
                SceneManager.LoadScene(1);
            }
        }
    }

}
