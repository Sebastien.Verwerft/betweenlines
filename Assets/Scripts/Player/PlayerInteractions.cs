﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerInteractions : MonoBehaviour
{

    private CameraMovement cameraMovement;
    private MenuManager menuManager;
    private PlayerMovement playerMovement;
    private LoadingScreen loadingScreen;
    private SubTitles subTitles;

    public InteractionWithPlayer interactionWithPlayer;

    public bool canInteract;
    public bool showInteractVfx;
    public bool openDoor;

    public delegate float MathMovementDelegate(float i);
    public delegate float MathRotationDelegate(float j);
    public delegate float MathRotationBackDelegate(float k);

    private GameObject interactionStaff;

    private MathMovementDelegate mathMovementDelegate;
    private MathRotationDelegate mathRotationDelegate;
    private MathRotationBackDelegate mathRotationBackDelegate;
    private Maths maths;

    private ShadersFading shadersFading;

    public TextMeshProUGUI textInteraction;
    public Image imageInteraction;

    public string tagInteraction;

    public InputActionAsset inputActions;

    public bool isPaused;

    private AudioSource audioSource;

    private TextMeshProUGUI textOnRight;

    public GameObject returnPauseMenuButton;

    public Animator commodeSpriteAnimator;
    public Animator receipeAndDresserSpriteAnimator;

    public AudioSource HGaudio;

    private AudioClip tempAudioClip;

    private bool isFirstTimeOven = true;

    // Start is called before the first frame update
    void Start()
    {
        showInteractVfx = true;

        maths = this.GetComponent<Maths>();
        cameraMovement = GameObject.Find("Camera").GetComponent<CameraMovement>();
        menuManager = GameObject.Find("MenuManager").GetComponent<MenuManager>();
        loadingScreen = GameObject.Find("MenuManager").GetComponent<LoadingScreen>();
        playerMovement = GetComponent<PlayerMovement>();
        shadersFading = GameObject.Find("ShaderMat").GetComponent<ShadersFading>();

        interactionStaff = GameObject.Find("InteractionManager").transform.GetChild(14).gameObject;
        interactionStaff.SetActive(false);

        subTitles = GameObject.Find("SubTitles").GetComponent<SubTitles>();
        audioSource = GameObject.Find("Audio Source").GetComponent<AudioSource>();

        if(SceneManager.GetActiveScene().name == "Seb Kitchen")
        {
            tempAudioClip = HGaudio.clip;
        }

    }

    public void ActivateInteraction(InputAction.CallbackContext context)
    {
        if (context.started && canInteract )
        {

            if(tagInteraction == "Staff")
            {
                menuManager.StartCoroutine(loadingScreen.LoadLevelAnim(8));
                return;
            }

            cameraMovement.isZooming = !cameraMovement.isZooming;
            shadersFading.StartCoroutine(shadersFading.EliottShader(cameraMovement.isZooming));
            
            if(cameraMovement.isZooming)
            {
                StartCoroutine(ShowHideTextInteraction(0.5f, cameraMovement.isZooming));
            }

            if (tagInteraction == "Commode" || tagInteraction == "Bed" || tagInteraction == "Recipes")
            {
                mathMovementDelegate = maths.CameraHermiteMix;
                mathRotationDelegate = maths.SmoothStop3;
                mathRotationBackDelegate = maths.Linear;

                if(tagInteraction == "Commode")
                {
                    if(cameraMovement.isZooming)
                    {
                        interactionWithPlayer.ObjectToAnimate[0].GetComponent<Animator>().SetTrigger("Trigger");
                        commodeSpriteAnimator.SetTrigger("Trigger");
                    }
                    else
                    {
                        commodeSpriteAnimator.SetTrigger("Trigger Reverse");
                        subTitles.StartCoroutine(subTitles.Sequence(0.2f,"Her baked goods were always delicious.", interactionWithPlayer.audioClip, 4f));
                    }
                }

                if(tagInteraction == "Bed")
                {
                    shadersFading.StartCoroutine(shadersFading.HGshader());
                    openDoor = true;
                    
                    if(cameraMovement.isZooming)
                    {
                        subTitles.StartCoroutine(subTitles.Sequence(2f,"If I recall correctly: Gretel 6, Hansel 4 ! \nShe was a great shot.", interactionWithPlayer.audioClip, 10f));
                        interactionWithPlayer.GetComponent<AudioSource>().Play();
                    }

                }

                if(tagInteraction == "Recipes")
                {
                    if(cameraMovement.isZooming)
                    {
                       for (int i = 0; i < interactionWithPlayer.ObjectToAnimate.Length; i++)
                       {
                            interactionWithPlayer.ObjectToAnimate[i].GetComponent<Animator>().SetTrigger("Trigger");
                       }

                       receipeAndDresserSpriteAnimator.SetTrigger("Trigger");
                       StartCoroutine(Wait(3f));
                    }
                    else
                    {
                        subTitles.StartCoroutine(subTitles.Sequence(0.2f, "This recipe requires a lot of ingredients… \nIt looks convoluted.", interactionWithPlayer.audioClip, 8f));
                        receipeAndDresserSpriteAnimator.SetTrigger("Trigger Reverse");
                        StartCoroutine(ShowHideTextUIRight(0.2f, false));
                    }
                }

            }
            else if (tagInteraction == "Books" || tagInteraction == "LivingRoomTable" || tagInteraction == "Broom")
            {
                mathMovementDelegate = maths.CameraHermiteMix;
                mathRotationDelegate = maths.SmoothStop3;
                mathRotationBackDelegate = maths.SmoothStop2;

                if (tagInteraction == "Books" && cameraMovement.isZooming)
                {
                    interactionWithPlayer.ObjectToAnimate[0].GetComponent<Animator>().SetTrigger("Trigger");
                    subTitles.StartCoroutine(subTitles.Sequence(2f, "I always wonder how this doesn’t collapse.", interactionWithPlayer.audioClip, 4f));
                }

                if (tagInteraction == "LivingRoomTable")
                {
                    shadersFading.StartCoroutine(shadersFading.HGshader());
                    openDoor = true;

                    if(cameraMovement.isZooming)
                    {
                        subTitles.StartCoroutine(subTitles.Sequence(2f, "Young people these days! No proper manners !", interactionWithPlayer.audioClip, 4f));
                        interactionWithPlayer.GetComponent<AudioSource>().Play();
                    }

                }

                if(tagInteraction == "Broom")
                {
                    interactionWithPlayer.transform.GetChild(1).gameObject.SetActive(cameraMovement.isZooming);

                    if(cameraMovement.isZooming)
                    {
                        subTitles.StartCoroutine(subTitles.Sequence(2f, "To be honest, i'm not sure about the efficiency of that broom...", interactionWithPlayer.audioClip, 6f));
                    }

                }

            }
            else if (tagInteraction == "Chair" || tagInteraction == "Oven")
            {
                mathMovementDelegate = maths.CameraHermiteMix;
                mathRotationDelegate = maths.SmoothStop3;
                mathRotationBackDelegate = maths.CameraChairMix;

                if(tagInteraction == "Chair")
                {
                    if(cameraMovement.isZooming)
                    {
                        subTitles.StartCoroutine(subTitles.Sequence(2f, "Will it ever stop ? There soon won’t be any wool left !", interactionWithPlayer.audioClip, 5f));
                        interactionWithPlayer.ObjectToAnimate[0].GetComponent<Animator>().SetTrigger("Trigger");
                        interactionWithPlayer.transform.Find("Sound").GetComponent<AudioSource>().Play();
                    }
                    else
                    {
                        interactionWithPlayer.transform.Find("Sound").GetComponent<AudioSource>().Stop();
                    }
                }

                if(tagInteraction == "Oven" && !cameraMovement.isEndCameraSpot)
                {
                    shadersFading.StartCoroutine(shadersFading.HGshader());
                    interactionStaff.SetActive(true);
                    subTitles.StartCoroutine(subTitles.Sequence(0.7f, "Hum, what are they up to in there? ", interactionWithPlayer.audioClip, 4f));
                    HGaudio.clip = tempAudioClip;
                    
                }

                if(cameraMovement.isEndCameraSpot)
                {
                    for (int i = 0; i < interactionWithPlayer.ObjectToAnimate.Length; i++)
                    {
                        interactionWithPlayer.ObjectToAnimate[i].GetComponent<Animator>().SetTrigger("Trigger");
                    }

                    if(isFirstTimeOven)
                    {
                        subTitles.StartCoroutine(subTitles.Sequence(1f, "Oh look ! Her cane ! It's moving on it's own !", interactionWithPlayer.audioClipOven, 4f));
                        isFirstTimeOven = false;
                    }

                }
            }

            else if(tagInteraction == "Dresser")
            {
                mathMovementDelegate = maths.CameraHermiteMix;
                mathRotationDelegate = maths.CameraDresserMix;
                mathRotationBackDelegate = maths.Linear;

                if(cameraMovement.isZooming)
                {
                    for (int i = 0; i < interactionWithPlayer.ObjectToAnimate.Length; i++)
                    {
                        interactionWithPlayer.ObjectToAnimate[i].GetComponent<Animator>().SetTrigger("Trigger");
                    }

                    subTitles.StartCoroutine(subTitles.Sequence(2f, "Never wrapped my head around how this kind of ingredients could yield such scrumptious results.", interactionWithPlayer.audioClip, 9f));
                    receipeAndDresserSpriteAnimator.SetTrigger("Trigger");
                    StartCoroutine(Wait(3f));
                }
                else
                {
                    receipeAndDresserSpriteAnimator.SetTrigger("Trigger Reverse");
                    StartCoroutine(ShowHideTextUIRight(0.2f, false));
                }

            }
            else if (tagInteraction == "Fireplace" || tagInteraction == "Teapot" || tagInteraction == "KitchenTable")
            {
                mathMovementDelegate = maths.CameraHermiteMix;
                mathRotationDelegate = maths.SmoothStop2;
                mathRotationBackDelegate = maths.Linear;

                if(cameraMovement.isZooming)
                {
                    for (int i = 0; i < interactionWithPlayer.ObjectToAnimate.Length; i++)
                    {
                        interactionWithPlayer.ObjectToAnimate[i].GetComponent<Animator>().SetTrigger("Trigger");
                    }

                    if (tagInteraction == "Fireplace")
                    {
                        subTitles.StartCoroutine(subTitles.Sequence(2f, "*sigh* This feels better.. Doesn't it ?", interactionWithPlayer.audioClip, 5f));
                    }

                    if(tagInteraction == "Teapot")
                    {
                        subTitles.StartCoroutine(subTitles.Sequence(2f, "Nothing beats a good cup of tea in front of a warm cosy fireplace.", interactionWithPlayer.audioClip, 6f));
                    }

                    if(tagInteraction == "KitchenTable")
                    {
                        subTitles.StartCoroutine(subTitles.Sequence(1f, "What about me ? Am I allowed some ?", interactionWithPlayer.audioClip, 5f));
                    }

                }

            }
            else if(tagInteraction == "Shell")
            {
                mathMovementDelegate = maths.CameraHermiteMix;
                mathRotationDelegate = maths.SmoothStop2;
                mathRotationBackDelegate = maths.SmoothStop2;

                if(cameraMovement.isZooming)
                {
                    subTitles.StartCoroutine(subTitles.Sequence(2f, "Thankfully Magic can clean this up !", interactionWithPlayer.audioClip, 3f));

                    for (int i = 0; i < interactionWithPlayer.ObjectToAnimate.Length; i++)
                    {
                        interactionWithPlayer.ObjectToAnimate[i].GetComponent<Animator>().SetTrigger("Trigger");
                    }
                }
            }

            cameraMovement.StartCoroutine(cameraMovement.LerpCameraMovement(cameraMovement.cameraSpotStart, interactionWithPlayer.cameraSpotReference, interactionWithPlayer.lerpDuration, mathMovementDelegate));
            cameraMovement.StartCoroutine(cameraMovement.LerpCameraRotation(interactionWithPlayer.lerpDuration, interactionWithPlayer.transform.GetChild(0).gameObject, interactionWithPlayer.lookAtGlobalPointView, mathRotationDelegate, mathRotationBackDelegate, interactionWithPlayer.lookAtSensitivity, interactionWithPlayer.lookAtRollbackSensitivity));

            playerMovement.currentSpeed = 0.0f;

            showInteractVfx = !showInteractVfx;
        }
    }

    public void SetInteractiveObject(GameObject go, TextMeshProUGUI text)
    {
        interactionWithPlayer = go.GetComponent<InteractionWithPlayer>();
        textOnRight = text;
    }

    public void SetGamePause(InputAction.CallbackContext context)
    {
        if(context.started)
        {
            isPaused = !isPaused;
            SetGamePause();
        }
    }

    public void SetGamePause()
    {
        if (isPaused)
        {
            Time.timeScale = 0;
            Cursor.visible = true;
            audioSource.Pause();
            inputActions.FindActionMap("Player").Disable();
            inputActions.FindActionMap("UI").Enable();
            menuManager.pauseMenu.SetActive(true);

            for (int i = 0; i < menuManager.allAudioSources.Length; i++)
            {
                menuManager.allAudioSources[i].Pause();
            }

            if (menuManager.currentScheme == "GamePad")
            {
                EventSystem.current.SetSelectedGameObject(returnPauseMenuButton);
            }
        }

        else if (!isPaused)
        {
            Time.timeScale = 1;
            Cursor.visible = false;
            audioSource.UnPause();
            EventSystem.current.SetSelectedGameObject(null);
            menuManager.pauseMenu.SetActive(false);
            inputActions.FindActionMap("Player").Enable();
            inputActions.FindActionMap("UI").Disable();

            for (int i = 0; i < menuManager.allAudioSources.Length; i++)
            {
                menuManager.allAudioSources[i].UnPause();
            }

        }
    }

    IEnumerator Wait(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        StartCoroutine(ShowHideTextUIRight(1f, true));
    }

    public IEnumerator ShowHideTextInteraction(float timeLerp, bool afficher)
    {
        float timeElapsed = 0.0f;
        float value;

        while (timeElapsed < timeLerp)
        {
            float t = timeElapsed / timeLerp;

            if (!afficher)
            {
                value = Mathf.Lerp(0, 1, t);
            }
            else
            {
                value = Mathf.Lerp(1, 0, t);
            }

            textInteraction.color = new Vector4(255, 255, 255, value);
            imageInteraction.color = new Vector4(255, 255, 255, value);

            if (value < 0.1f)
            {
                textInteraction.color = new Vector4(255, 255, 255, 0);
                imageInteraction.color = new Vector4(255, 255, 255, 0);
            }

            timeElapsed += Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator ShowHideTextUIRight(float timeLerp, bool afficher)
    {
        float timeElapsed = 0.0f;
        float value = 0.0f;

        while (timeElapsed < timeLerp)
        {
            float t = timeElapsed / timeLerp;

            if (afficher)
            {
                value = Mathf.Lerp(0, 1, t);
            }
            else
            {
                value = Mathf.Lerp(1, 0, t);
            }

            textOnRight.color = new Vector4(textOnRight.color.r, textOnRight.color.g, textOnRight.color.b, value);

            if (value < 0.1f)
            {
                textOnRight.color = new Vector4(textOnRight.color.r, textOnRight.color.g, textOnRight.color.b, 0);
            }

            timeElapsed += Time.deltaTime;
            yield return null;
        }
    }
}
