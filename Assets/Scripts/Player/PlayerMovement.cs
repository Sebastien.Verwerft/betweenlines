﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    /////////////////////
    /// Private values
    /////////////////////
    private Rigidbody rb;

    private Vector3 movementX;
    private Vector3 movementZ;
    private Vector3 newPosition;
    private Vector3 direction;

    private Vector2 movementInputValues;

    private bool walking = false;

    public bool waitOneFrame;

    /////////////////////
    /// Public values
    /////////////////////
    public Animator animator;
    public float moveSpeed;
    public float currentSpeed;

    public bool beginAnimEnded = false;

    // Start is called before the first frame update
    private void Start()
    {
        //set the starting pos where the gameobject is
        newPosition = transform.position;
        currentSpeed = moveSpeed;

        if(SceneManager.GetActiveScene().name != "Seb Bedroom")
        {
            StartCoroutine(Wait(3.0f));
        }

        // getting the rigidbody
        rb = GetComponent<Rigidbody>();
    }

    // for every physics related things
    private void FixedUpdate()
    {
        direction = (movementX + movementZ) * currentSpeed * Time.deltaTime;

        if(movementInputValues == Vector2.zero)
        {
            rb.velocity = Vector3.zero;
        }

        // Defining the new position
        newPosition = transform.position + direction;

        // Move the player
        rb.MovePosition(newPosition);

        // Rotating the character where he's moving
        transform.LookAt(newPosition);

        if (direction != Vector3.zero)
        {
            walking = true;
            animator.SetBool("Walk", walking);

            //if(!gameObject.GetComponent<AudioSource>().isPlaying)
            //{
            //    gameObject.GetComponent<AudioSource>().Play();
            //}

        }
        else
        {
            walking = false;
            animator.SetBool("Walk", walking);

            //if (gameObject.GetComponent<AudioSource>().isPlaying)
            //{
            //    gameObject.GetComponent<AudioSource>().Stop();
            //}
        }
    }

    // Store the value from the input system
    public void GetMovementValues(InputAction.CallbackContext context)
    {
        movementInputValues = context.ReadValue<Vector2>();
        
        if(beginAnimEnded)
        {
            // redirecting the movement relative to the camera axis
            movementX = Camera.main.transform.right * movementInputValues.x;
            movementZ = new Vector3(Camera.main.transform.forward.x, 0, Camera.main.transform.forward.z).normalized * movementInputValues.y;
        }
    }

    public IEnumerator Wait(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        beginAnimEnded = true;
    }

}