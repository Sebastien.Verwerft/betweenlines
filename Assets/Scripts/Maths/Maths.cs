﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Maths : MonoBehaviour
{
    public float Linear(float t)
    {
        return t;
    }

    public float SmoothStart2(float t)
    {
        return t * t;
    }

    public float SmoothStart3(float t)
    {
        return t * t * t;
    }

    public float SmoothStart4(float t)
    {
        return t * t * t * t;
    }

    public float SmoothStart5(float t)
    {
        return t * t * t * t * t;
    }


    public float SmoothStop2(float t)
    {
        return 1 - ((1 - t) * (1 - t));
    }

    public float SmoothStop3(float t)
    {
        return 1 - ((1 - t) * (1 - t) * (1 - t));
    }

    public float SmoothStop4(float t)
    {
        return 1 - ((1 - t) * (1 - t) * (1 - t) * (1 - t));
    }

    public float SmoothMix(float a, float b, float c)
    {
        if(c >= 0f || c <= 1f)
        {
            return (c * a) + ((1-c) * b);
        }
        else
        {
            return 0;
        }
    }

    public float Hermite(float t)
    {
        return t * t * (3f - 2f * t);
    }

    public float CameraHermiteMix(float t)
    {
        return Hermite(SmoothMix(Linear(t),SmoothStop2(t), 0.3f));
    }

    public float CameraChairMix(float t)
    {
        return SmoothMix(Linear(t), SmoothStart2(t), 0.5f);
    }

    public float CameraDresserMix(float t)
    {
        return SmoothMix(SmoothStop2(t), SmoothStop3(t), 0.5f);
    }

    public Vector3 BezierCurve(float value, Vector3 p0, Vector3 p1, Vector3 p2)
    {
        return (1.0f - value) * (1.0f - value) * p0 + 2.0f * (1.0f - value) * value * p1 + value * value * p2;
    }

    public Vector3 BezierCurveReverse(float value, Vector3 p0, Vector3 p1, Vector3 p2)
    {
        return (1.0f - value) * (1.0f - value) * p2 + 2.0f * (1.0f - value) * value * p1 + value * value * p0;
    }
}
