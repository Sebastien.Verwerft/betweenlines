﻿using UnityEngine;

public class PlaySoundInAnim : MonoBehaviour
{
    void Sound(AudioClip source)
    {
        GetComponent<AudioSource>().clip = source;
        GetComponent<AudioSource>().Play();
    }
}
