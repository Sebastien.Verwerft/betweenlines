﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundsInScene : MonoBehaviour
{
    private AudioSource audioSource;

    public SubTitles subTitles;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();

        if(SceneManager.GetActiveScene().name == "Seb LivingRoom")
        {
            subTitles.StartCoroutine(subTitles.Sequence(2f, "That room is as welcoming and warm as ever.", audioSource.clip, 4f));
            //GameObject.FindGameObjectWithTag("Music").GetComponent<MusicBetweenScenes>().UnPauseMusic();
        }
        else if(SceneManager.GetActiveScene().name == "Seb Kitchen")
        {
            subTitles.StartCoroutine(subTitles.Sequence(2f, "*Sniffs* Now this is the place where the magic happens !", audioSource.clip, 6f));
            //GameObject.FindGameObjectWithTag("Music").GetComponent<MusicBetweenScenes>().UnPauseMusic();
        }

    }

    public void PlaySound(AudioClip tempClip)
    {
        audioSource.clip = tempClip;
        audioSource.Play();
    }
}
